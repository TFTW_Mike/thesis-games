﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddButtons : MonoBehaviour {

	[SerializeField]
	private Transform puzzleField; //to move, rotate object

	[SerializeField]
	private GameObject btn; 

	void Awake() {
		for (int i = 0; i < 8; i++) {
			GameObject button = Instantiate (btn); // create copy of btn and assign it to button
			button.name = "" + i;
			button.transform.SetParent (puzzleField, false);
		}
	}
}
