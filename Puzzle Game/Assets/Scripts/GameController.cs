﻿using System.Collections;
using System.Collections.Generic; // we will use list
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	[SerializeField]
	private Sprite bgImage;

	public Sprite[] puzzles;

	public List<Sprite> gamePuzzles = new List<Sprite> ();

	public List<Button> btns = new List<Button>(); // create a list, not an array, bcuz you can modify the value in a list

	private bool firstGuess, secondGuess;

	private int countGuesses;
	private int countCorrectGuesses;
	private int gameGuesses;

	private int firstGuessIndex, secondGuessIndex;
	private string firstGuessPuzzle, secondGuessPuzzle;

	/*--------------------------------------------------------------------------------------------------------------------*/


	void Awake() {
		puzzles = Resources.LoadAll<Sprite> ("Sprites/Candy"); // load all the sprites in the path
	}

	void Start() {
		GetButtons ();
		AddListeners ();
		AddGamePuzzles ();
		Shuffle (gamePuzzles);
		gameGuesses = gamePuzzles.Count / 2;
	}

	void GetButtons() {
		GameObject[] objects = GameObject.FindGameObjectsWithTag ("PuzzleButton"); // find all game objects that has a PuzzleButton tag

		for (int i = 0; i < objects.Length; i++) {
			btns.Add (objects[i].GetComponent<Button>()); // get button component
			btns[i].image.sprite = bgImage; // access button to change the background image
		}
	}

	void AddGamePuzzles() {
		int looper = btns.Count; // looper - how many button elements in a list of button
		int index = 0;

		for(int i = 0; i < looper; i++) {
			if (index == looper / 2) { 
				index = 0; // reset to reset from the start
			}

			gamePuzzles.Add (puzzles [index]); // add same buttons 2x
			index++;
		}
	}

	void AddListeners() { 
		// for (int i = 0; btns.Count; i++) {} //return how many buttons we have

		foreach (Button btn in btns) { // better way	 when modyifing a list
			btn.onClick.AddListener (() => PickAPuzzle()); // adding our listener to button
		}
	}

	public void PickAPuzzle() {
		//string name = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name; // get the name of the selected game object
		//Debug.Log ("Button =" + name);

		if (!firstGuess) { // if not guess for the first time, set to true to not execute this code again
			firstGuess = true;
			firstGuessIndex = int.Parse (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name); // parse - convert string into integer, parsing the name of our button
			firstGuessPuzzle = gamePuzzles[firstGuessIndex].name;
			btns[firstGuessIndex].image.sprite = gamePuzzles[firstGuessIndex];

		} else if (!secondGuess) { // second guess
			secondGuess = true;
			secondGuessIndex = int.Parse (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name); // parse - convert string into integer, parsing the name of our button
			secondGuessPuzzle = gamePuzzles[secondGuessIndex].name;
			btns[secondGuessIndex].image.sprite = gamePuzzles[secondGuessIndex];

			/* check if the first & second guess match
			if (firstGuessPuzzle == secondGuessPuzzle) {
				Debug.Log ("The Puzzles Match");
			} else {
				Debug.Log ("The Puzzles Don't Match");
			}
			*/

			countGuesses++; // increment the number of guesses that we did
			StartCoroutine (CheckIfThePuzzlesMatch ()); // check if puzzles match
		}
	}

	IEnumerator CheckIfThePuzzlesMatch() { // this is coroutine - delayed behavior
		yield return new WaitForSeconds(1f);

		if (firstGuessPuzzle == secondGuessPuzzle) {
			yield return new WaitForSeconds (0.5f);

			btns [firstGuessIndex].interactable = false; // disable the button if we have guessed correctly
			btns [secondGuessIndex].interactable = false; // disable the button if we have guessed correctly

			btns [firstGuessIndex].image.color = new Color (0, 0, 0, 0); //make buttons not visible
			btns [secondGuessIndex].image.color = new Color (0, 0, 0, 0); //make buttons not visible

			CheckIfTheGameIsFinished (); // check if puzzle finished

		} else {
			yield return new WaitForSeconds (0.5f);

			btns [firstGuessIndex].image.sprite = bgImage; // set background image back to our button
			btns [secondGuessIndex].image.sprite = bgImage; // set background image back to our button
		}

		yield return new WaitForSeconds (0.5f);
		firstGuess = secondGuess = false; // reset first and second guess
	}

	void CheckIfTheGameIsFinished() {
		countCorrectGuesses++; // increment correct guesses

		if (countCorrectGuesses == gameGuesses) {
			Debug.Log ("Game Finished");
			Debug.Log ("It took you " + countGuesses + " many guess(es) to finish the game");
		}
	}

	void Shuffle(List<Sprite> list) { // randomize our objects
		for(int i = 0; i < list.Count; i++) {
			Sprite temp = list [i]; // create sprite for temporary variable to store the elements that is on the index[i]
			int randomIndex = Random.Range (i, list.Count); // return between i and list.count
			list [i] = list [randomIndex];
			list [randomIndex] = temp;
		}
	}
}
