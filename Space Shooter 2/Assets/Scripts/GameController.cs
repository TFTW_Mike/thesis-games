﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public static GameController instance;

	public Text scoreText;
	private int score;

	void Start () {
		score = 0;
		UpdateScore ();
	}
	  
	public void SetScore (int newscore) {
		score += newscore;
		UpdateScore ();
	}
		
	public void UpdateScore() {
		scoreText.text = "" + score;
	}

	public int GetScore() {
		return this.score;
	}
}