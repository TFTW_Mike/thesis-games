﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour {

	public static GameOverManager instance; // instance

	private GameObject scoreText;
	private GameObject GameOver, MenuPause;

	private Text finalScore, finalScoreShadow;
	bool paused;

	void Awake() {
		paused = !paused;
		GameOver = GameObject.Find ("GameOverBody");
		MenuPause = GameObject.Find ("MenuPause");

		scoreText = GameObject.Find ("Score Text");
		finalScore = GameObject.Find ("Final Score").GetComponent<Text> (); // get reference of the final score
	}

	void FixedUpdate () {
		//		paused = !paused;
		MenuPause.SetActive (false); //AL
		Time.timeScale = 1;
		paused = !paused;


	}

	public void GameOverShowPanel() {
		finalScore.text = "" + GameController.instance.GetScore ();
	}

//	void InitializeVariables() {
//		GameOver = GameObject.Find ("GameOverBody");
//		MenuPause = GameObject.Find ("MenuPause");
//
//		scoreText = GameObject.Find ("Score Text");
//		finalScore = GameObject.Find ("Final Score").GetComponent<Text> (); // get reference of the final score
//	}

	public void Restart() {

		SceneManager.LoadScene (1);
		GameOver.SetActive (false);
		MenuPause.SetActive (false); //AL
		Time.timeScale = 1;
	}
}
