﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour {

	public Button ButtonResume;
	public GameObject Canvas;
	GameObject MenuPause;
	GameObject GameOver;
	bool paused;
	bool muted;
	[SerializeField]

		
	void Awake () {
		paused = !paused;
		MenuPause = GameObject.Find ("MenuPause"); //AL
		GameOver = GameObject.Find ("GameOverBody");
//		Button btn = ButtonResume.GetComponent<Button> ();
//		btn.onClick.AddListener (Resume);
//		Canvas.gameObject.SetActive (false);
	}
		
	void FixedUpdate () {
//		paused = !paused;
		MenuPause.SetActive (false); //AL
		Time.timeScale = 1;
		paused = !paused;


	}
		
	public void PauseGame() {
		paused = !paused;

		if (paused) {
			MenuPause.SetActive (true); //AL
			Time.timeScale = 0;

		} else if (!paused) {
			MenuPause.SetActive (false); //AL
			Time.timeScale = 1;
		}
	}

	public void Resume() {
		if (paused == true) {
			paused = false;
			MenuPause.SetActive (false); //AL
			Time.timeScale = 1;
		} else {
			MenuPause.SetActive (false); //AL
			Time.timeScale = 1;
		} 

		paused = !paused;
	}

	public void MainMenu() {
		SceneManager.LoadScene (0);

	}

	public void Restart() {

		SceneManager.LoadScene (1);
		GameOver.SetActive (false);
		MenuPause.SetActive (false); //AL
		Time.timeScale = 1;

		}

	public void GOverRestart() {
		SceneManager.LoadScene (1);
		if (GameOver.activeSelf) {
			GameOver.SetActive (false);
			MenuPause.SetActive (false); //AL
			Time.timeScale = 1;
		}

	}

	public void Mute() {

		if (muted == false) {
			AudioListener.volume = 1;

		} else if (muted == true) {
			AudioListener.volume = 0;
		}

			muted = !muted;
	}

	public void Quit() {
		SceneManager.LoadScene (0);
		MenuPause.SetActive (false); //AL
		Time.timeScale = 1;
	}

}
