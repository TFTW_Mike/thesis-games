﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour {

	public Vector3 bulletOffset = new Vector3 (0, 0.5f, 0);

	[SerializeField]
	private AudioSource audioSource;

	[SerializeField]
	private AudioClip herogunshot;

	public GameObject bulletPrefab;

	int bulletLayer;

//	public bool doubleShot;
//	public GameObject Shot;// this is the shot prefab
//	public Transform ShotPoint1; //for trishot and double shot
//	public Transform ShotPoint2; //same as the other line

			
	public float fireDelay = 0.15f;
	float coolDownTimer = 0;

	void Start() {
		bulletLayer = gameObject.layer;
	}

	void Update () {
		coolDownTimer -= Time.deltaTime;

		if (Input.GetButton("Fire1") && coolDownTimer <= 0) {

//			if (doubleShot) {
//				FireBullet (); // we are gonna create this in a second
//				StartCoroutine("DoubleCountdown", 0); // we are gonna create this
//			}
			 //SHOOT
			coolDownTimer = fireDelay;
			audioSource.PlayOneShot(herogunshot);

			Vector3 offset = transform.rotation * bulletOffset;

			Instantiate (bulletPrefab, transform.position + offset, transform.rotation);
		}
	}

//	void OnTriggerEnter2D (Collider2D other) {
//		if (other.tag == "DoubleShot") {
//			doubleShot = true;// if we collide with powerup it starts
//		}
//	}

//	void FireBullet() {
//		//Extre bullet 1
//		GameObject Bullet1 = (GameObject)Instantiate (Shot); // We create from nothing a shot prefab
//		Bullet1.transform.position = ShotPoint1.transform.position;// We shoot it at shotpoint
//		//Extre bullet 2
//		GameObject Bullet2 = (GameObject)Instantiate (Shot); // the same logic of the previous l
//		Bullet2.transform.position = ShotPoint2.transform.position;
//
//	}
//
//	IEnumerator DoubleCountdown() {
//		yield return new WaitForSeconds (10f); // How long can last the double shot?
//		doubleShot = false;// Then it come back to normal
//
//	}
}
