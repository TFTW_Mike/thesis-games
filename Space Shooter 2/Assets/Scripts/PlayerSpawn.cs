﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSpawn : MonoBehaviour {

	public static PlayerSpawn instance;

	[SerializeField]
	private AudioSource audioSource;

	[SerializeField]
	private AudioClip spawn;

	public GameObject playerPrefab;
	GameObject playerInstance;
	GameObject GameOver;

	bool paused;

	public Sprite heart1;
	public Sprite heart2;

	public Image Heart1;
	public Image Heart2;
	public Image Heart3;
	public Image Heart4;
	public Image Heart5;

	public int numLives = 5;
	public int score = 0;

	public GUIText scoreText;

	float respawnTimer;

	void Awake() {
		Initialize ();
	}

	void Initialize() {
		GameOver = GameObject.Find ("GameOverBody");
	}
	// Use this for initialization
	void Start () {
		GameOver.SetActive (false);
		SpawnPlayer ();
		//GameOver = GameObject.Find ("GameOver");
	}

	void SpawnPlayer() {
		audioSource.PlayOneShot(spawn);
		numLives--;
		respawnTimer = 1;
		playerInstance = (GameObject)Instantiate (playerPrefab, transform.position, Quaternion.identity);
	}
	// Update is called once per frame
	void Update () {
		if (playerInstance == null && numLives > 0) {
			respawnTimer -= Time.deltaTime;

			if (respawnTimer <= 0) {
				SpawnPlayer ();
				UpdateHeartMeter ();
			}
		}
	}

	public void OnGUI() {
		if (numLives > 0 || playerInstance != null) {
			//GUI.Label (new Rect (0, 0, 100, 50), "");
			GameOver.SetActive (false);
		} else {
			//GUI.Label (new Rect (Screen.width / 2 - 50, Screen.height / 2 - 25, 100, 50), "");
			GameOver.SetActive (true);
			Time.timeScale = 0;
		}
	}

	public void UpdateHeartMeter() {

		switch (numLives) {

		case 4:
			Heart1.sprite = heart2;
			Heart2.sprite = heart2;
			Heart3.sprite = heart2;
			Heart4.sprite = heart2;
			Heart5.sprite = heart2;
			return;

		case 3:
			Heart1.sprite = heart2;
			Heart2.sprite = heart2;
			Heart3.sprite = heart2;
			Heart4.sprite = heart2;
			Heart5.sprite = heart1;
			return;

		case 2:
			Heart1.sprite = heart2;
			Heart2.sprite = heart2;
			Heart3.sprite = heart2;
			Heart4.sprite = heart1;
			Heart5.sprite = heart1;
			return;

		case 1:
			Heart1.sprite = heart2;
			Heart2.sprite = heart2;
			Heart3.sprite = heart1;
			Heart4.sprite = heart1;
			Heart5.sprite = heart1;
			return;

		case 0:
			Heart1.sprite = heart2;
			Heart2.sprite = heart1;
			Heart3.sprite = heart1;
			Heart4.sprite = heart1;
			Heart5.sprite = heart1;
			return;

		default:
			Heart1.sprite = heart1;
			Heart2.sprite = heart1;
			Heart3.sprite = heart1;
			Heart4.sprite = heart1;
			Heart5.sprite = heart1;
			return;

		}
	}

}