﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagedByCollisions : MonoBehaviour {

	public int health = 1;

	float invulnTimer = 0;

	int correctLayer;

	SpriteRenderer spriteRend;

	public int scoreValue;

	private GameController gameController;

	[SerializeField]
	private AudioSource audioSource;

	[SerializeField]
	private AudioClip herodied;

	private float timewhendied;

	private GameObject playership;

//	public bool doubleShot;
//	public GameObject Shot;// this is the shot prefab
//	public Transform ShotPoint1; //for trishot and double shot
//	public Transform ShotPoint2; //same as the other line

	void Awake() {
		Initialzie ();
	}

	void Initialzie() {
		playership = GameObject.FindWithTag ("Player");
	}

	void Start () {
		correctLayer = gameObject.layer;

		GameObject gameControllerObject = GameObject.FindGameObjectWithTag ("GameController");

		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent<GameController> ();
		}

		if (gameController == null) {
			Debug.Log ("Cannot find 'GameController' script");
		}

		// NOTE! This only get the renderer on the parent object/
		// In other words, it doesn't work for children. IE "enemy01"

		spriteRend = GetComponent<SpriteRenderer> ();

		if (spriteRend == null) {
			spriteRend = transform.GetComponentInChildren<SpriteRenderer> ();
		}
			if (spriteRend == null) {
				Debug.LogError ("Object '" + gameObject.name + "' has no sprite renderer.");
			}
	}

	void OnTriggerEnter2D(Collider2D other) {
		Debug.Log ("Trigger");
		health--;
		invulnTimer = 0f;
		gameObject.layer = 8;

		gameController.SetScore (scoreValue);

		//		if (other.tag == "DoubleShot") {
		//			doubleShot = true;// if we collide with powerup it starts
		//		}t
	}
	
	// Update is called once per frame
	void Update () {
//		if (doubleShot) {
//			FireBullet (); // we are gonna create this in a second
//			StartCoroutine("DoubleCountdown", 0); // we are gonna create this
//		}

		if (invulnTimer > 0) {
			invulnTimer -= Time.deltaTime;

			if (invulnTimer <= 0) {
				gameObject.layer = correctLayer;
				if (spriteRend != null) {
					spriteRend.enabled = true;
				}
			} else {
				if (spriteRend != null) {
					spriteRend.enabled = !spriteRend.enabled;
				}
			}
		}
		if (health <= 0) {
			//audioSource.PlayOneShot(herodied);
			Die ();
		}
	}

	void Die() {
			timewhendied += Time.deltaTime;
			//Debug.Log (timewhendied);

		if (timewhendied > 0.1f) {
			AudioSource audio = GameObject.Find("DiedPlsWork").GetComponent<AudioSource> ();
			audio.Play ();
				Destroy(gameObject);
				//audioSource.PlayOneShot(herodied);
			}
		}

//	void FireBullet() {
//		//Extre bullet 1
//		GameObject Bullet1 = (GameObject)Instantiate (Shot); // We create from nothing a shot prefab
//		Bullet1.transform.position = ShotPoint1.transform.position;// We shoot it at shotpoint
//		//Extre bullet 2
//		GameObject Bullet2 = (GameObject)Instantiate (Shot); // the same logic of the previous l
//		Bullet2.transform.position = ShotPoint2.transform.position;
//
//	}
//
//	IEnumerator DoubleCountdown() {
//		yield return new WaitForSeconds (10f); // How long can last the double shot?
//		doubleShot = false;// Then it come back to normal
//
//	}
}
