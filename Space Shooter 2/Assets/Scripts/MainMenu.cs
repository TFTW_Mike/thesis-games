﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	bool muted;

	public void StartGame() {
		SceneManager.LoadScene (1);

	}

	public void Exit() {
		Application.Quit();
	}

	public void Mute() {

		if (muted == false) {
			AudioListener.volume = 1;

		} else if (muted == true) {
			AudioListener.volume = 0;
		}

		muted = !muted;
	}
}
