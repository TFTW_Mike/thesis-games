﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShipMovement : MonoBehaviour {

	float maxSpeed = 9f;
	float rotSpeed = 250f;

	float shipBoundaryRadius = 0.5f;

	// Use this for initialization -
	void Start () {
		
	}
	
	// Update is called once per frame -
	void Update () {
		
		// Grab our rotation quaternion -
		Quaternion rot = transform.rotation;

		// Grab the Z euler angle
		float z = rot.eulerAngles.z;

		// Grab the Z angle based on input
		z -= Input.GetAxis("Horizontal") * rotSpeed * Time.deltaTime;

		// Recreate the quaternion
		rot = Quaternion.Euler( 0, 0, z );

		// Feed the quaternion into our action 
		transform.rotation = rot;

		Vector3 pos = transform.position;

		// Moving the Ship -
		Vector3 velocity = new Vector3(0, Input.GetAxis ("Vertical") * maxSpeed * Time.deltaTime, 0);

		pos += rot * velocity;

		/* Another way of Moving the Ship -
		transform.Translate (new Vector3 (0, Input.GetAxis ("Vertical") * maxSpeed * Time.deltaTime, 0)); */

		// RESTRICT the player to the camera's boundaries!

		// First to vertical, because its simpler
		if (pos.y+shipBoundaryRadius > Camera.main.orthographicSize) {
			pos.y = Camera.main.orthographicSize - shipBoundaryRadius;
		}

		if (pos.y-shipBoundaryRadius < -Camera.main.orthographicSize) {
			pos.y = -Camera.main.orthographicSize + shipBoundaryRadius;
		}
			
		// Now calculate the orthographic width based on the screen ratio
		float screenRatio = (float)Screen.width / (float)Screen.height;
		float widthOrtho = Camera.main.orthographicSize * screenRatio;

		// Now do the horizontal bounds
		if (pos.x+shipBoundaryRadius > widthOrtho) {
			pos.x = widthOrtho - shipBoundaryRadius;
		}

		if (pos.x - shipBoundaryRadius < -widthOrtho) {
			pos.x = -widthOrtho + shipBoundaryRadius;
		}

		//Finally our position!
		transform.position = pos;
	}
}
