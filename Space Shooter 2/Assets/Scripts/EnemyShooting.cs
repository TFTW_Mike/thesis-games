﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour {

	[SerializeField]
	private AudioSource audioSource;

	[SerializeField]
	private AudioClip enemygunshot;

	public Vector3 bulletOffset = new Vector3 (0, 0.5f, 0);

	public GameObject bulletPrefab;

	int bulletLayer;

	public float fireDelay = 0.25f;
	float coolDownTimer = 0;

	Transform player;

	void Start() {
		bulletLayer = gameObject.layer;
	}

	void Update () {

		if ( player == null ) {
			// FInd the player's ship!
			GameObject go = GameObject.FindWithTag ( "Player" );

			if (go != null) {
				player = go.transform;
			}
		}

		coolDownTimer -= Time.deltaTime;

		if (coolDownTimer <= 0 && player != null && Vector3.Distance(transform.position, player.position)< 7 ) {
			// SHOOT
			coolDownTimer = fireDelay;
			audioSource.PlayOneShot(enemygunshot);

			Vector3 offset = transform.rotation * bulletOffset;

			GameObject bulletGO = (GameObject)Instantiate (bulletPrefab, transform.position + offset, transform.rotation);
			bulletGO.layer = gameObject.layer;
		}
	}
}
