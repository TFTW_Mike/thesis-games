﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectibes2 : MonoBehaviour {

	public LvlManager lvlman;
	public int Points = 1;


	// Use this for initialization
	void Start () {
		lvlman = FindObjectOfType<LvlManager> ();
	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Monster") {

			lvlman.AddPoints (Points);
			Destroy (this.gameObject);
		}
	}
}
