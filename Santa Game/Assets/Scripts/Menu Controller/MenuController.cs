﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour {

	public static MenuController instance;

	public bool muted;

	[SerializeField]
	private GameObject[] characters;

	private bool isGCharacterUnlocked, isRCharacterUnlocked;

	void Awake () {
		MakeInstance ();
	}

	void Start () {
		// Comment here
		//characters [GameController.instance.GetSelectedCharacter ()].SetActive (true);
		//CheckifCharactersAreUnlocked ();
	}

	void Update() {
		if (muted == false) {
			AudioListener.volume = 1;

		} else if (muted == true) {
			AudioListener.volume = 0;
		}
	}

	void MakeInstance() {
		if (instance == null) {
			instance = this;
		}
	}

	void CheckifCharactersAreUnlocked() {
		if (GameController.instance.IsRCharacterUnlocked () == 1) {
			isRCharacterUnlocked = true;
		}

		if (GameController.instance.IsGCharacterUnlocked () == 1) {
			isGCharacterUnlocked = true;
		}
	}

	// fade animation
	public void PlayGame() {
		SceneFader.instance.FadeIn ("Gameplay");
	}

	public void ChangeCharacter() {
		if (GameController.instance.GetSelectedCharacter () == 0) {
			if (isGCharacterUnlocked) {
				characters [0].SetActive (false);
				GameController.instance.SetSelectedCharacter (1);
				characters [GameController.instance.GetSelectedCharacter ()].SetActive (true);
			}
		} else if (GameController.instance.GetSelectedCharacter () == 1) {
			if (isRCharacterUnlocked) {
				characters [1].SetActive (false);
				GameController.instance.SetSelectedCharacter (2);
				characters [GameController.instance.GetSelectedCharacter ()].SetActive (true);
			} else {
				characters [1].SetActive (false);
				GameController.instance.SetSelectedCharacter (0);
				characters [GameController.instance.GetSelectedCharacter ()].SetActive (true);
			}
		} else if (GameController.instance.GetSelectedCharacter () == 2) {
			characters [2].SetActive (false);
			GameController.instance.SetSelectedCharacter (0);
			characters [GameController.instance.GetSelectedCharacter ()].SetActive (true);
		}
	}

	public void Mute() {
		muted = !muted;
	}
}
