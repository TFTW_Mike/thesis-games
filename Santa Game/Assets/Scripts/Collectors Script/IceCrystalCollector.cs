﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceCrystalCollector : MonoBehaviour {

	private GameObject[] iceCrystalHolders;
	private float distance = 3f;
	private float lastIceCrystalX;
	private float iceCrystalMin = -0.9f;
	private float iceCrystalMax = 1.30f;

	void Awake() {
		iceCrystalHolders = GameObject.FindGameObjectsWithTag ("IceCrystalHolder");

		// to randomize the ice crystal position
		for (int i = 0; i < iceCrystalHolders.Length; i++) {
			Vector3 temp = iceCrystalHolders [i].transform.position;
			temp.y = Random.Range (iceCrystalMin, iceCrystalMax);
			iceCrystalHolders [i].transform.position = temp;
		}

		lastIceCrystalX = iceCrystalHolders [0].transform.position.x;

		for (int i = 1; i < iceCrystalHolders.Length; i++) {
			if (lastIceCrystalX < iceCrystalHolders [i].transform.position.x) {
				lastIceCrystalX = iceCrystalHolders [i].transform.position.x;
			}
		}
	}

	void OnTriggerEnter2D(Collider2D target) {
		if (target.tag == "IceCrystalHolder") {
			Vector3 temp = target.transform.position;

			temp.x = lastIceCrystalX + distance;
			temp.y = Random.Range (iceCrystalMin, iceCrystalMax);

			target.transform.position = temp;
			lastIceCrystalX = temp.x;
		}
	}
}
