﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneFader : MonoBehaviour {

	public static SceneFader instance;

	[SerializeField]
	private GameObject fadeCanvas;

	[SerializeField]
	private Animator fadeAnim;

	void Awake() {
		MakeSingleton ();
	}

	void MakeSingleton() {
		if (instance != null) {
			Destroy (gameObject);
		} else {
			instance = this;
			DontDestroyOnLoad (gameObject);
		}
	}

	//start our 2 courotines below 
	public void FadeIn(string levelName) {
		StartCoroutine (FadeInAnimation (levelName));
	}

	public void FadeOut() {
		StartCoroutine (FadeOutAnimation());
	}

	// Play our fade in animation
	IEnumerator FadeInAnimation(string levelName) {
		fadeCanvas.SetActive (true);
		fadeAnim.Play ("FadeIn");
		yield return StartCoroutine(MyCoroutine.WaitForRealSeconds(0.3f));
		Application.LoadLevel (levelName);
		FadeOut ();
	}

	// Play our fade out animation
	IEnumerator FadeOutAnimation() {
		fadeAnim.Play ("FadeOut");
		yield return StartCoroutine(MyCoroutine.WaitForRealSeconds(0.5f));
		fadeCanvas.SetActive (false);
	}
}
