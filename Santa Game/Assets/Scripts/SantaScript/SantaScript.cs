﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SantaScript : MonoBehaviour {

	// Create static instance to access all the public methods and variable on this script, 
	// so we dont need to get a reference to other scripts
	public static SantaScript instance;

	[SerializeField] // Get Rigidbody2D Component - More efficient way
	private Rigidbody2D myRigidBody; // Reference to Rigidbody so we can bounce Santa

	// Reference to Animator
	[SerializeField] // Get Animator Component 
	private Animator anim;

	private float forwardSpeed = 3;
	private float bounceSpeed = 3.5f;

	private bool didFly;
	public bool isAlive;

	private Button flyButton;

	[SerializeField]
	private AudioSource audioSource;

	[SerializeField]
	private AudioClip flyClick, pointClip, diedClip;

	public int score;

	void Awake() {
		//myRigidBody = GetComponent<Rigidbody2D> (); // Get Rigidbody2D Component - Less efficient way

		if (instance == null) {
			instance = this;
		}

		isAlive = true;
		score = 0;

		flyButton = GameObject.FindGameObjectWithTag ("FlyButton").GetComponent<Button> ();
		flyButton.onClick.AddListener (() => flySanta ());

		SetCamerasX ();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	/* Update is called once per frame
	void Update () {
		
	}
	*/

	// FixedUpdate is called ever 2 or 3 frames
	void FixedUpdate () {
		if (isAlive) {
			Vector3 temp = transform.position;
			temp.x += forwardSpeed * Time.deltaTime;
			transform.position = temp;

			if (didFly) {
				didFly = false;
				myRigidBody.velocity = new Vector2 (0, bounceSpeed);
				audioSource.PlayOneShot(flyClick);
				anim.SetTrigger ("SantaFly");
			}

			if (myRigidBody.velocity.y >= 0) {
				transform.rotation = Quaternion.Euler (0, 0, 0);
			} else {
				float angle = 0;
				angle = Mathf.Lerp (0, -90, -myRigidBody.velocity.y / 7);
				transform.rotation = Quaternion.Euler (0, 0, angle);
			}
		}
	}

	void SetCamerasX() {
		CameraScript.offsetX = ((Camera.main.transform.position.x - transform.position.x) - 1f);
	}
	public float GetPositionX() {
		return transform.position.x;
	}
	
	// To fly santa
	public void flySanta() {
		didFly = true;
	}

	void OnTriggerEnter2D(Collider2D target) {
		if (target.tag == "IceCrystalHolder") {
			score++;
			GameplayController.instance.SetScore (score);
			audioSource.PlayOneShot (pointClip);
		}
	}

	// So we will not set the trigger per collider objects
	void OnCollisionEnter2D(Collision2D target) {
		if(target.gameObject.tag == "Ground" || target.gameObject.tag == "IceCrystal") {
			if(isAlive) {
				isAlive = false;
				anim.SetTrigger("SantaHit");
				audioSource.PlayOneShot(diedClip);
				GameplayController.instance.PlayerDiedShowScore (score);
			}
		}
	}
}
