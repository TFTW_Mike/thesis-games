﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameplayController : MonoBehaviour {

	public static GameplayController instance;

	[SerializeField]
	private Text scoreText, endScore, bestScore, gameOverText;

	[SerializeField]
	private Button restartGameButton, instructionsButton;

	[SerializeField]
	private GameObject pausePanel;

	[SerializeField]
	private Image starImage;

	[SerializeField]
	private GameObject[] characters;

	[SerializeField]
	private Sprite[] stars;

	[SerializeField]
	private Image starsImage;

	void Awake() {
		MakeInstance ();
		Time.timeScale = 0f;
	}

	void Start() {

	}

	void MakeInstance() {
		if (instance == null) { 
			instance = this;
		}
	}

	public void Pausegame() {
		if (SantaScript.instance != null) { // check if the game started or not
			if (SantaScript.instance.isAlive) { // check if Santa is alive
				pausePanel.SetActive(true);
				gameOverText.gameObject.SetActive (false);
				starsImage.gameObject.SetActive (false);
				endScore.text = "" + SantaScript.instance.score;
				bestScore.text = "" + GameController.instance.GetHighScore ();
				Time.timeScale = 0f; // reset all
				restartGameButton.onClick.RemoveAllListeners();
				restartGameButton.onClick.AddListener (() => ResumeGame ()); // listener for resume game
			}
		}
	}

	public void GoToMenuButton() {
		SceneFader.instance.FadeIn ("MainMenu");
	}

	public void ResumeGame() {
		pausePanel.SetActive (false);
		Time.timeScale = 1f;
	}

	public void RestartGame() {
		SceneFader.instance.FadeIn (Application.loadedLevelName);
	}

	public void PlayGame() {
		scoreText.gameObject.SetActive (true);
		characters [GameController.instance.GetSelectedCharacter ()].SetActive (true);
		instructionsButton.gameObject.SetActive (false);
		Time.timeScale = 1f;
	}

	public void SetScore(int score) {
		scoreText.text = "" + score;
	}

	public void PlayerDiedShowScore(int score) {
		pausePanel.SetActive (true); //show score to user or go back to main menu or play again
		gameOverText.gameObject.SetActive (true); // game over
		starsImage.gameObject.SetActive (true);
		scoreText.gameObject.SetActive (false); // deactivate score

		endScore.text = "" + score; // show score

		if (score > GameController.instance.GetHighScore ()) {
			GameController.instance.SetHighScore (score);
		}

		bestScore.text = "" + GameController.instance.GetHighScore ();

		//giving stars based on score
		if (score == 0) {
			starsImage.gameObject.SetActive (false);
		} else if (score <= 10) {
			starsImage.sprite = stars [0];
		} else if (score > 10 && score < 20) {
			starsImage.sprite = stars [1];

			if (GameController.instance.IsGCharacterUnlocked () == 0) {
				GameController.instance.UnlockGCharacter ();
			}
		} else {
			starsImage.sprite = stars [2];

			if (GameController.instance.IsGCharacterUnlocked () == 0) {
				GameController.instance.UnlockGCharacter ();
			}

			if (GameController.instance.IsRCharacterUnlocked () == 0) {
				GameController.instance.UnlockRCharacter ();
			}
		}

		restartGameButton.onClick.RemoveAllListeners ();
		restartGameButton.onClick.AddListener (() => RestartGame ());
	}
}
