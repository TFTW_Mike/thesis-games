﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	public static GameController instance;

	private const string HIGH_SCORE = "High Score";
	private const string SELECTED_CHARACTER = "Selected Character";
	private const string G_CHARACTER = "G Character";
	private const string R_CHARACTER = "R Character";

	void Awake() {
		MakeSingleton ();
		IsTheGameStartedForTheFirstTime ();
		//PlayerPrefs.DeleteAll ();
	}

	// Use this for initialization
	void Start () {

	}

	void MakeSingleton() {
		if (instance != null) {
			Destroy (gameObject);
		} else {
			instance = this;
			DontDestroyOnLoad (gameObject);
		}
	}

	void IsTheGameStartedForTheFirstTime() {
		if (!PlayerPrefs.HasKey ("IsTheGameStartedForTheFirstTime")) {
			PlayerPrefs.SetInt (HIGH_SCORE, 0);
			PlayerPrefs.SetInt (SELECTED_CHARACTER, 0);
			PlayerPrefs.SetInt (G_CHARACTER, 1);
			PlayerPrefs.SetInt (R_CHARACTER, 1);
			PlayerPrefs.SetInt ("IsTheGameStartedForTheFirstTime", 0);
		}
	}

	public void SetHighScore(int score) {
		PlayerPrefs.SetInt (HIGH_SCORE, score);
	}

	public int GetHighScore() {
		return PlayerPrefs.GetInt (HIGH_SCORE);
	}

	public void SetSelectedCharacter(int selectedCharacter) {
		PlayerPrefs.SetInt (SELECTED_CHARACTER, selectedCharacter);
	}

	public int GetSelectedCharacter() {
		return PlayerPrefs.GetInt (SELECTED_CHARACTER);
	}

	public void UnlockGCharacter() {
		PlayerPrefs.SetInt (G_CHARACTER, 1);
	}

	public int IsGCharacterUnlocked() {
		return PlayerPrefs.GetInt (G_CHARACTER);
	}

	public void UnlockRCharacter() {
		PlayerPrefs.SetInt (R_CHARACTER, 1);
	}

	public int IsRCharacterUnlocked() {
		return PlayerPrefs.GetInt (R_CHARACTER);
	}

}
