﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

	public static float offsetX;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (SantaScript.instance != null) {
			if (SantaScript.instance.isAlive) {
				MoveTheCamera ();
			}
		}
	}

	// The camera will follow Santa's position 
	void MoveTheCamera() {
		Vector3 temp = transform.position;
		temp.x = SantaScript.instance.GetPositionX () + offsetX;
		transform.position = temp;
	}
}
