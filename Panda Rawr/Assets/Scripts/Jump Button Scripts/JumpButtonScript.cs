﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class JumpButtonScript : MonoBehaviour, IPointerDownHandler, IPointerUpHandler { // interfaces that will tell us if we holding down the button and releasing the button

	public void OnPointerDown(PointerEventData data) {
			//Debug.Log ("We are touching the button");
			if (PlayerJumpScript.instance != null) {
				PlayerJumpScript.instance.SetPower (true);
			}
	}

	public void OnPointerUp(PointerEventData data) {
		//Debug.Log ("We have released the button");
		if (PlayerJumpScript.instance != null) {
			PlayerJumpScript.instance.SetPower (false);
		}
	}
}
