﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerJumpScript : MonoBehaviour {

	public static PlayerJumpScript instance; //instance of the scipt to set the jump power

	private Rigidbody2D myBody; // mas of the player
	private Animator anim; // for animation 

	[SerializeField]
	private float forceX, forceY; 

	private float tersholdX = 7f;
	private float tersholdY = 14f;

	private bool setPower, didJump;

	private Slider powerBar;
	private float powerBarTreshold = 10f;
	private float powerBarValue = 0f;

	private GameObject canjump;
	private bool grounded, enoughpower;

	private float timeonground = 0f;
	private bool okaytolerp;

	void Awake() {
		MakeInstance ();
		Initialize ();
	}

	void Update() {
		SetPower (); // update setPower

		timeonground = timeonground + 0.01f;
		//Debug.Log (timeonground);

		if (timeonground > 2.0f) {
			okaytolerp = true;
			//Debug.Log ("you can lerp now");
		}
	}

	void Initialize() { // get game component references
		canjump = GameObject.Find("Jump Button");
		powerBar = GameObject.Find("Power Bar").GetComponent<Slider>();
		myBody = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();

		powerBar.minValue = 0;
		powerBar.maxValue = 10;

		powerBar.value = powerBarValue;

	}

	void MakeInstance() {
		if (instance == null) {
			instance = this; // set the instance to PlayerJumpScript
		}
	}

	void SetPower() {
			if (setPower) {
				forceX += tersholdX * Time.deltaTime;
				forceY += tersholdY * Time.deltaTime;

				if (forceX > 6.5f) {
					forceX = 6.5f;
				}

				if (forceY > 13.5f) {
					forceY = 13.5f;
				}

				powerBarValue += powerBarTreshold * Time.deltaTime;
				powerBar.value = powerBarValue;
			}
	}

	public void SetPower(bool setPower) {
			this.setPower = setPower;
			//Debug.Log(powerBar.value);

			// test if the SetPower has been triggered from the JumpButtonScript or not
			/* 
			if (setPower) {
				Debug.Log ("We are setting the power");
			} else {
				Debug.Log ("We are not setting the power");
			}
			*/
			if (!setPower) {
			if (powerBar.value > 4.0f) {
				Jump ();
				grounded = false;
				canjump.SetActive (false);
			} else {
				powerBar.value = 0f;
				powerBarValue = 0f; // when jump reset value
				powerBar.value = powerBarValue;
				forceX = forceY = 0f; // reset forces, else they will be the same
			}
		}

	}

	void Jump() {
		anim.SetBool ("Jump", true);
		myBody.velocity = new Vector2 (forceX, forceY);
		forceX = forceY = 0f; // reset forces, else they will be the same
		didJump = true;

		//anim.SetBool ("Jump", didJump);
		powerBarValue = 0f; // when jump reset value
		powerBar.value = powerBarValue;
	}


	void OnTriggerEnter2D(Collider2D target) {
		if (didJump) { // check if we're touching the platform after jumping
			didJump = false;
			//anim.SetBool ("Jump", didJump);

			if (target.tag == "Platform") { // check if the the tag is on the platform
				timeonground += Time.deltaTime;
				//Debug.Log("Landed on the platform after jumping");
				if (GameManager.instance != null) {
					timeonground = 0f;
					GameManager.instance.CreateNewPlatformAndLerp (target.transform.position.x); 
					anim.SetBool ("Jump", false);
				}

				if (ScoreManager.instance != null) {
					ScoreManager.instance.IncrementScore ();
				}
			}
		}

		if (target.tag == "Dead") {
			if (GameOverManager.instance != null) {
				GameOverManager.instance.GameOverShowPanel ();
				anim.SetBool ("Jump", false);
			}
			Destroy (gameObject);
		}
	}

	void OnCollisionEnter2D(Collision2D target) {
		if (target.gameObject.tag == "Platform") {
			grounded = true;
			canjump.SetActive (true);
			//Debug.Log ("were touching the ground");
		}
	}


}
