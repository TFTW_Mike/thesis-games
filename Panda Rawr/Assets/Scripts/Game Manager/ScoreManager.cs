﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

	public static ScoreManager instance;

	private Text scoreText, scoreTextShadow;

	private int score;

	void Awake () {
		scoreText = GameObject.Find ("Score Text").GetComponent<Text> ();
		scoreTextShadow = GameObject.Find ("Score Text Shadow").GetComponent<Text> ();
		MakeInstance ();
	}

	void MakeInstance () {
		if (instance == null) {
			instance = this;
		}
	}

	public void IncrementScore() { // increment our score every new platform 
		score++;
		scoreText.text = "" + score;
		scoreTextShadow.text = "" + score;
	}

	public int GetScore() { // return our score
		return this.score;
	}
}
