﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; // use our UI elements

public class GameOverManager : MonoBehaviour {

	public static GameOverManager instance; // instance

	private GameObject gameOverPanel;
	private GameObject blurBackground;
	private Animator gameOverAnim;

	private Button playAgainBtn, backBtn;

	private GameObject scoreText;
	private Text finalScore, finalScoreShadow;

	AudioSource firstbg, secondbg, thirdbg;

	void Awake() {
		MakeInstance ();
		InitializeVariables (); // calling the function
		gameOverPanel.SetActive(false);
		blurBackground.SetActive (false);
	}

	void MakeInstance() {
		if (instance == null) {
			instance = this;
		}
	}

	public void GameOverShowPanel() {
		scoreText.SetActive (false); // hide the current score
		gameOverPanel.SetActive (true);
		blurBackground.SetActive (true);

		finalScore.text = "" + ScoreManager.instance.GetScore ();
		finalScoreShadow.text = "" + ScoreManager.instance.GetScore ();
		gameOverAnim.Play ("FadeIn");

		firstbg = GameObject.Find("Background Music 01").GetComponent<AudioSource>();
	}

	void InitializeVariables() {
		gameOverPanel = GameObject.Find ("Game Over Panel Holder"); // find our game over panel game object
		gameOverAnim = gameOverPanel.GetComponent<Animator> (); // get our animation component
		blurBackground = GameObject.Find("Blur Background");

		playAgainBtn = GameObject.Find ("Play Again Button").GetComponent<Button> (); // find our play again button game object
		backBtn = GameObject.Find ("Back Button").GetComponent<Button> (); // find our play back button game object

		playAgainBtn.onClick.AddListener (() => PlayAgain ()); // add listener to our play button
		backBtn.onClick.AddListener (() => BackToMenu ()); // add listener to our back button

		scoreText = GameObject.Find ("Score Text");
		finalScore = GameObject.Find ("Final Score").GetComponent<Text> (); // get reference of the final score
		finalScoreShadow = GameObject.Find ("Final Score Shadow").GetComponent<Text> (); // get reference of the final score
	}

	public void PlayAgain() {
		Application.LoadLevel (Application.loadedLevelName); // load our gameplay scene
	}

	public void BackToMenu() {
		Application.LoadLevel ("MainMenu"); // load our Main Menu scene
	}
}
