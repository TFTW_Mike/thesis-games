﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public static GameManager instance;

	[SerializeField]
	private GameObject player;

	[SerializeField]
	private GameObject platform;

	[SerializeField]
	private GameObject blurbackground;

	private float minX = -2.35f, maxX = 2.0f, minY = -2.2f, maxY = -1.2f;

	private bool lerpCamera;
	private float lerpTime = 9f;
	private float lerpNow;
	private float lerpX;

	AudioSource firstbg;

	void Awake() {
		Initialize ();
		MakeInstance ();
		CreateInitialPlatforms (); // create initial platform when we start a game
		blurbackground.SetActive(false);
	}

	void Update() {
		if (lerpCamera) {
			LerpCamera ();
		}
	}

	void MakeInstance() {
		if (instance == null) {
			instance = this;
		}
	}

	void Initialize() {
		firstbg = GameObject.Find("Background Music 01").GetComponent<AudioSource>();
	}

	void CreateInitialPlatforms() {
		Vector3 temp = new Vector3(Random.Range(minX, minX + 1.2f), Random.Range(minY, maxY), 0); 
		Instantiate (platform, temp, Quaternion.identity); // intantiate a platform
		temp.y += 2f;
		Instantiate (player, temp, Quaternion.identity);
		temp = new Vector3(Random.Range(maxX, maxX - 1.2f), Random.Range(minY, maxY), 0); 
		Instantiate (platform, temp, Quaternion.identity); // intantiate a platform
	}

	void LerpCamera() { //lerp our camera
		float x = Camera.main.transform.position.x; //get camera reference x position

		x = Mathf.Lerp (x, lerpX, lerpTime * Time.deltaTime); // going to x and lerpx, using the delta time

		Camera.main.transform.position = new Vector3 (x, Camera.main.transform.position.y, Camera.main.transform.position.z);

		if (Camera.main.transform.position.x >= (lerpX - 0.07f)) {
			lerpCamera = false;
		}
	}

	public void CreateNewPlatformAndLerp(float lerpPosition) {
		CreateNewPlatform ();
		lerpX = lerpPosition + maxX;  
		lerpCamera = true;
	}

	void CreateNewPlatform() { //create new platform
		float cameraX = Camera.main.transform.position.x;
		float newMaxX = (maxX * 2) + cameraX;
		Instantiate(platform, new Vector3(Random.Range(newMaxX, newMaxX - 1.2f), Random.Range(maxY, maxY - 1.2f), 0), Quaternion.identity);
	}
}
