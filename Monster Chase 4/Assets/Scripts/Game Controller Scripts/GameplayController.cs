﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayController : MonoBehaviour {

	bool paused;

	void Start() {
		paused = !paused;
	}

	void OnEnable() {
		Player.playerDied += PlayerDied;
	}

	void OnDisable() {
		Player.playerDied -= PlayerDied;
	}

	void PlayerDied() {
		StartCoroutine (RestartGame ());
	}

	IEnumerator RestartGame() {
		yield return new WaitForSeconds (1.5f);
		SceneManager.LoadScene ("Gameplay");
	}
	// Update is called once per frame
	public void GoToMainMenu () {
		SceneManager.LoadScene ("MainMenu");
	}

	public void PauseGame() {

		paused = !paused;
		if (paused == true) {
			Time.timeScale = 0;
		} else {
			Time.timeScale = 1;
		}
	}
}
