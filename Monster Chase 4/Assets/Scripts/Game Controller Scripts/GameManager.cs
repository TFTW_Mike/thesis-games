﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static GameManager instance;

	[SerializeField]
	private GameObject[] players;

	[HideInInspector]
	public int index;

	void Awake () {
		MakeSingleton ();
	}

	void OnLevelWasLoaded () {
		if (SceneManager.GetActiveScene().name == "Gameplay") {
			Instantiate (players [index], new Vector3 (0, -3, 0), Quaternion.identity);
		}
	}

	void MakeSingleton() {
		if (instance != null) {
			// destroy duplicate
			Destroy (gameObject);
		} else {
			instance = this;
			DontDestroyOnLoad (this);
		}
	}
}
