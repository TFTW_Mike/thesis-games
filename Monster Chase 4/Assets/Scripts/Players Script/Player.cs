﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	public float moveForce = 10f;
	public float jumpForce = 700f;
	public float maxVelocity = 22f;

	private Rigidbody2D myBody;
	private Animator anim;

	private bool grounded;

	GameObject whenPlayerDied;

	public delegate void PlayerDied();
	public static event PlayerDied playerDied;

	void Awake () {
		myBody = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		whenPlayerDied = GameObject.Find ("WhenPlayerDied");
	}

	void FixedUpdate () {
		PlayerMoveKeyboard ();
	}

	public void PlayerMoveKeyboard() {
		float forceX = 0f;
		float forceY = 0f;

		float vel = Mathf.Abs (myBody.velocity.x);

		float h = Input.GetAxisRaw ("Horizontal");

		if (h > 0) {
			if (vel < maxVelocity)
				forceX = moveForce;

			Vector3 temp = transform.localScale;
			temp.x = 1f;
			transform.localScale = temp;

			anim.SetBool ("Walk", true);

		} else if (h < 0) {

			if (vel < maxVelocity)
				forceX = -moveForce;

			Vector3 temp = transform.localScale;
			temp.x = -1f;
			transform.localScale = temp;

			anim.SetBool ("Walk", true);

		} else {
			anim.SetBool ("Walk", false);
		}

		if (Input.GetKey (KeyCode.Space)) {
			if(grounded) {
				grounded = false;
				forceY = jumpForce;
			}
		}

		myBody.AddForce (new Vector2 (forceX, forceY));
	}

	void OnCollisionEnter2D(Collision2D target) {
		if (target.gameObject.tag == "Ground") {
			grounded = true;
		}

		if (target.gameObject.tag == "Monster") {
			
			if (playerDied != null) {
				playerDied ();
//				whenPlayerDied.SetActive (true);
			}

			Destroy (gameObject);

		}

	}

	void OnTriggerEnter2D(Collider2D target) {
		if (target.tag == "Monster") {
			
			if (playerDied != null) {
				playerDied ();
//				whenPlayerDied.SetActive (true);
			}

			Destroy (gameObject);

		}
	}
}
