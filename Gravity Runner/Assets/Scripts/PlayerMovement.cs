﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	public static PlayerMovement instance;

	private Button jumpBtn;

	private Rigidbody2D myBody;

	private float speed = 4.5f;

	void Awake() {
		jumpBtn = GameObject.Find("Jump").GetComponent<Button>(); // find "Jump" game object
		jumpBtn.onClick.AddListener(()=>Jump());
		myBody = GetComponent<Rigidbody2D>(); // get component to our rigidbody that is attached to our player
	}

	// Update is called once per frame
	void Update() {
		Vector3 temp = transform.position;
		temp.x += speed * Time.deltaTime;
		transform.position = temp;
	}

	public void Jump() {
		myBody.gravityScale *= -1; // change gravity to go up and down
		Vector3 temp = transform.localScale; // change the scale of the player to go up and down
		temp.y *= -1; // change the scale of the player to go up and down
		transform.localScale = temp; // change the scale of the player to go up and down
	}
}
