﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerScore : MonoBehaviour {

	public static PlayerScore instance;

	private Text scoreText;
	private int score = 0;

	void Awake() {
		MakeInstance ();
		scoreText = GameObject.Find("ScoreText").GetComponent<Text>(); // get referemce of our score
		scoreText.text = "0"; // set initial score value
	}

	void MakeInstance() {
		if (instance == null) {
			instance = this;
		}
	}

	void OnTriggerEnter2D(Collider2D target) { // detect collision to our coin and rocket
		if(target.tag == "Coin") {				// check if our coin collide to our player
			//score++; 							// increase score by 1
			//scoreText.text = score.ToString(); // convert our score to string

			if (ScoreManager.instance != null) {
				ScoreManager.instance.IncrementScore ();
			}

			target.gameObject.SetActive(false); // deactivate our coin
		}

		if(target.tag == "Rocket") {					// check if our rocket collide to our player
			transform.position = new Vector3(0,1000,0);	// move our player
			target.gameObject.SetActive(false);			// deactivate our rocket
			StartCoroutine(RestartGame());				// start coroutine because all of the codes after you deactivate our player will not work
		}
	}

	IEnumerator RestartGame() {		// restart our game
		yield return new WaitForSecondsRealtime(0.3f); // wait in seconds to restart our scene
		//SceneManager.LoadScene(SceneManager.GetActiveScene().name); // restart our scene
		if (GameOverManager.instance != null) {
			GameOverManager.instance.GameOverShowPanel ();
		}
		Destroy (gameObject);
	}
}
