﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aura : MonoBehaviour {

	private GameObject aura;
	private GameObject player;
	private GameObject speech1;
	private GameObject speech2;
	private GameObject speech3;
	private GameObject speech4;
	private GameObject speech5;

	void Awake() {
		aura = GameObject.Find ("Aura");
		aura.SetActive (false);
		player = GameObject.Find ("Player");
		player.SetActive (false);
		speech1 = GameObject.Find ("Speech1");
		speech1.SetActive (false);
		speech2 = GameObject.Find ("Speech2");
		speech2.SetActive (false);
		speech3 = GameObject.Find ("Speech3");
		speech3.SetActive (false);
		speech4 = GameObject.Find ("Speech4");
		speech4.SetActive (false);
		speech5 = GameObject.Find ("Speech5");
		speech5.SetActive (false);
	}

	// Use this for initialization
	void Start() {
		StartCoroutine (ShowAura ());
	}
		
	IEnumerator ShowAura() {
		yield return new WaitForSecondsRealtime(0.45f);
		aura.SetActive (true);
		yield return new WaitForSecondsRealtime(0.4f);
		player.SetActive (true);
		yield return new WaitForSecondsRealtime(2.6f);
		aura.SetActive (false);
		speech1.SetActive (true);
		yield return new WaitForSecondsRealtime(2.2f);
		speech1.SetActive (false);
		yield return new WaitForSecondsRealtime(0.5f);
		speech2.SetActive (true);
		yield return new WaitForSecondsRealtime(2.0f);
		speech2.SetActive (false);		
		yield return new WaitForSecondsRealtime(1.5f);
		speech3.SetActive (true);
		yield return new WaitForSecondsRealtime(1.2f);
		speech3.SetActive (false);
		yield return new WaitForSecondsRealtime(1.7f);
		speech4.SetActive (true);
		yield return new WaitForSecondsRealtime(0.8f);
		speech4.SetActive (false);
		yield return new WaitForSecondsRealtime(3.5f);
		speech5.SetActive (true);
		yield return new WaitForSecondsRealtime(0.8f);
		speech5.SetActive (false);
	}
}
