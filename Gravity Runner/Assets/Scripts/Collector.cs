﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collector : MonoBehaviour {
	private float width = 0f;

	// Use this for initialization
	void Awake() {
		width = GameObject.Find("BG").GetComponent<BoxCollider2D>().size.x; // getting the width of the background
	}

	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D target) {
		if(target.tag == "BG" || target.tag == "Ground") { // check if we are colliding to our ground or background 
			Vector3 temp = target.transform.position;	// loop our background and ground
			temp.x += width * 3;						// loop our background and ground
			target.transform.position = temp;			// loop our background and ground
		}

		if(target.tag == "Coin" || target.tag == "Rocket") { // check if we are colliding to our coin or rocket
			target.gameObject.SetActive(false); // deactivate our target
		}
	}
}
