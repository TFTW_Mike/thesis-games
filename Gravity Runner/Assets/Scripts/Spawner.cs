﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
	
	[SerializeField]
	private GameObject[] items;

	private float minY = -2.5f, maxY = 2.5f; // minimum and maximum y position of our items

	// Use this for initialization
	void Start() {
			StartCoroutine (SpawnItems (17)); // Wait 1 second
	}


	IEnumerator SpawnItems(float time) { // spawn items
			yield return new WaitForSecondsRealtime (time); // WaitForSeconds - afftected by TimeScale | WaitForSecondsRealtime - unscaled time
			
			Vector3 temp;

		if (PauseManager.instance.paused == false) {
			 temp = new Vector3 (transform.position.x, Random.Range (minY, maxY), 0); 
			// transfer the position of our items where we want to spawn it and pass it to variable "temp"

			Instantiate (items [Random.Range (0, items.Length)], temp, Quaternion.identity); 
			// Explanation above code
			// Instantiate(items[Random.Range(0, items.Length)] - create new object between the 2 items ( coin, rocket)
			// temp - position where we want to spawn our items
			// Quaternion.identity - rotation (default - 0,0,0)
		} else {
			temp = new Vector3 (transform.position.x, 0, 0); 
			// transfer the position of our items where we want to spawn it and pass it to variable "temp"
		}

			StartCoroutine (SpawnItems (Random.Range (1f, 2f))); // Making a infinite Coroutine to loop
	}
}
