﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuSpawner : MonoBehaviour {

	[SerializeField]
	private GameObject[] items;

	private float minY = -0.55f, maxY = 2.5f; // minimum and maximum y position of our items

	private GameObject speech1;
	private GameObject speech2;
	private GameObject speech3;
	private GameObject speech4;

	void Awake() {
		speech1 = GameObject.Find ("Speech1");
		speech2 = GameObject.Find ("Speech2");
		speech3 = GameObject.Find ("Speech3");
		speech4 = GameObject.Find ("Speech4");
		speech1.SetActive (false);
		speech2.SetActive (false);
		speech3.SetActive (false);
		speech4.SetActive (false);
	}

	// Use this for initialization
	void Start() {
		StartCoroutine(SpawnItems(0.2f)); // Wait 1 second
		StartCoroutine (ShowSpeech ());
	}

	IEnumerator SpawnItems(float time) { // spawn items
		yield return new WaitForSecondsRealtime(time); // WaitForSeconds - afftected by TimeScale | WaitForSecondsRealtime - unscaled time
		Vector3 temp = new Vector3(transform.position.x, Random.Range(minY, maxY), 0); 
		// transfer the position of our items where we want to spawn it and pass it to variable "temp"

		Instantiate(items[Random.Range(0, items.Length)], temp, Quaternion.identity); 
		// Explanation above code
		// Instantiate(items[Random.Range(0, items.Length)] - create new object between the 2 items ( coin, rocket)
		// temp - position where we want to spawn our items
		// Quaternion.identity - rotation (default - 0,0,0)

		StartCoroutine(SpawnItems(Random.Range(1f, 2f))); // Making a infinite Coroutine to loop
	}

	IEnumerator ShowSpeech() {
		yield return new WaitForSecondsRealtime(1.5f);
		speech1.SetActive (true);
		yield return new WaitForSecondsRealtime(1.3f);
		speech1.SetActive (false);
		yield return new WaitForSecondsRealtime(0.7f);
		speech2.SetActive (true);
		yield return new WaitForSecondsRealtime(0.8f);
		speech2.SetActive (false);
		yield return new WaitForSecondsRealtime(2f);
		speech3.SetActive (true);
		yield return new WaitForSecondsRealtime(1f);
		speech3.SetActive (false);
		yield return new WaitForSecondsRealtime(4.5f);
		speech4.SetActive (true);
		yield return new WaitForSecondsRealtime(5f);
		speech4.SetActive (false);
	}
}
