﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseManager : MonoBehaviour {

	public static PauseManager instance;

	private GameObject pausePanel;
	private Animator pauseAnim;

	private Button pauseBtn, resumeBtn, menuBtn, muteBtn, exitBtn;
	public bool paused = false;

	private GameObject scoreText;
	private GameObject spawner;


	void Awake() {
		Time.timeScale = 1f;
		MakeInstance ();
		InitializeVariables ();
	}

	void MakeInstance() {
		if (instance == null) {
			instance = this;
		}
	}

	public void PauseShowPanel() {
		pausePanel.SetActive (true);

		pauseAnim.Play ("FadeIn");
	}

	void InitializeVariables() {
		pausePanel = GameObject.Find ("Pause Panel Holder");
		pauseAnim = pausePanel.GetComponent<Animator> ();

		pauseBtn = GameObject.Find ("Pause Button").GetComponent<Button> ();

		resumeBtn = GameObject.Find ("Resume Button").GetComponent<Button>();
		menuBtn = GameObject.Find ("Main Menu Button").GetComponent<Button> ();
		muteBtn = GameObject.Find ("Mute Button").GetComponent<Button> ();
		exitBtn = GameObject.Find ("Exit Button").GetComponent<Button> ();

		spawner = GameObject.Find ("Spawner");

		//pauseBtn.onClick.AddListener (()=>PauseGame());


		resumeBtn.onClick.AddListener (()=>ResumeGame());
		menuBtn.onClick.AddListener (()=>BackToMenuGame());
		muteBtn.onClick.AddListener (()=>MuteGame());
		exitBtn.onClick.AddListener (()=>ExitGame());

		scoreText = GameObject.Find ("ScoreText");

		pausePanel.SetActive (false);
	}

	void Update() {
		pauseBtn.onClick.AddListener (()=>Paused());
	}

	/*
	public void PauseGame() {
		PauseShowPanel ();
	}
	*/

	public void Paused() {
		PauseShowPanel ();
		StartCoroutine (PlsPaused ());
		paused = true;
	}

	IEnumerator PlsPaused() {
		yield return new WaitForSecondsRealtime(0.15f);
		if (Time.timeScale == 1f) {
			Time.timeScale = 0f;
		}
	}

	public void ResumeGame() {
		pausePanel.SetActive (false);
		Time.timeScale = 1f;
		paused = false;
	}

	public void BackToMenuGame() {
		Application.LoadLevel ("MainMenu");
		Time.timeScale = 1f;
	}

	public void MuteGame() {

	}

	public void ExitGame() {
		Application.Quit ();
	}
}
