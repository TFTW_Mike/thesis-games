﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket2 : MonoBehaviour {

	private float speed = 5f; // speed our rocket

	// Update is called one per frame
	void Update() {
		Vector3 temp = transform.position; // position of our rocket
		temp.x -= speed * Time.deltaTime; // speed of our rocket
		transform.position = temp; // position of our rocket
	}
}
