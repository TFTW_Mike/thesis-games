﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour {

	private float speed = 4.5f; // move the camera

	// Update is called once per frame  
	void Update() {
		Vector3 temp = transform.position; // Vector3 - three axis (x,y,z) | Vector2 - 2 axis (x,y)
		temp.x += speed * Time.deltaTime; // move the camera
		transform.position = temp; // move the camera
	}
}
